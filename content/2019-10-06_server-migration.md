---
title: Server migration
date: '2019-10-06T18:43:20.896Z'
severity: partial-outage
affectedsystems:
  - api
  - cdn
  - dns
  - site-delivery
resolved: true
---
Company's decision to migrate all existing services to containerized micro-services architecture hit in scaled out clusters caused a downtime.

<!--- language code: en -->
