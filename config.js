module.exports = {
  "title": "Oversight Service Status",
  "name": "oversight_status",
  "description": "Latest status of all services and micro-services run across entire Oversight ecosystem.",
  "defaultLocale": "en",
  "baseUrl": "https://status.oversight.in",
  "locales": [
    {
      "code": "en",
      "iso": "en-US",
      "name": "English",
      "file": "en.json"
    }
  ],
  "content": {
    "frontMatterFormat": "yaml"
  }
}
